package com.sourceit.firstandroidproject.landscape;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sourceit.firstandroidproject.R;
import com.sourceit.firstandroidproject.list.Article;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by wenceslaus on 23.01.18.
 */

public class ItemFragment extends Fragment {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.text)
    TextView text;
    Article article;

    public static ItemFragment newInstance() {
        return new ItemFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item, container, false);
        ButterKnife.bind(this, view);
        if (article != null) {
            updateUI();
        }
        return view;
    }

    public void updateArticle(Article article) {
        this.article = article;
        if (isResumed()) {
            updateUI();
        }
    }

    private void updateUI() {
        title.setText(article.getTitle());
        text.setText(article.getText());
    }

}

package com.sourceit.firstandroidproject.landscape;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sourceit.firstandroidproject.R;
import com.sourceit.firstandroidproject.list.Article;
import com.sourceit.firstandroidproject.recyclerlist.ArticleRecyclerAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by wenceslaus on 23.01.18.
 */

public class ListFragment extends Fragment {

    @BindView(R.id.list)
    RecyclerView list;

    ArticleRecyclerAdapter adapter;
    ElementClickListener elementClickListener;
    List<Article> articles;


    public static ListFragment newInstance() {
        return new ListFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ElementClickListener) {
            elementClickListener = (ElementClickListener) context;
        } else {
            throw new RuntimeException("Must be MultipleOrientationActivity");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, view);

        adapter = new ArticleRecyclerAdapter(articles, getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);

        adapter.setOnArticleClick(new ArticleRecyclerAdapter.OnArticleClick() {
            @Override
            public void onItemClick(int position) {
                Article article = adapter.getItem(position);
                elementClickListener.onItemClick(article);
            }
        });
        return view;
    }

    public void setList(List<Article> articles) {
        this.articles = articles;
    }
}
